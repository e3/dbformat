# dbformat

Utility to auto-format EPICS database files (in-place) according to [ESS database style guidelines](https://confluence.esss.lu.se/x/2PLPFg).

This tool does NOT work with JSON links. Do not use it if you have embedded JSON on your database files.

Please not that DB031 will not be verified or fixed, this rule needs a linter to be implemented.

## Quickstart

Python 3.6 or greater is required.

```sh
$ pip3 install --user dbformat -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
$ dbformat -h
```

## Pre-commit hook

```yaml
repos:
  - repo: https://gitlab.esss.lu.se/e3/dbformat
    rev: 0.5.3
    hooks:
      - id: dbformat
```
