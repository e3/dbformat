import json
from pathlib import Path
from typing import Any, Dict

import pytest

from dbformat import apply_rules

JSON_FILE = Path(__file__).parent.parent / "dbformat" / "guidelines.json"


def get_rule_info(entry) -> str:
    """Helper function to pretty-print ``pytest`` test cases."""
    return f"{entry['id']}: {entry['name']}"


@pytest.mark.parametrize(
    "item",
    json.loads(JSON_FILE.read_text())["guidelines"],
    ids=get_rule_info,
)
def test_rule(item: Dict[str, Any]):
    if not item["rule"]:
        pytest.skip("Not a rule")
    try:
        test_case = item["test_case"]
        if not test_case:
            pytest.skip("Test case for rule not implemented")
        if "initial" in test_case:
            actual, error = apply_rules(test_case["initial"])
            assert actual == test_case["expected"]
            assert not error
        else:
            actual, error = apply_rules(test_case["fails"])
            assert not error
    except KeyError:
        pytest.xfail("Rule not implemented")


@pytest.mark.parametrize(
    "initial, expected",
    [
        (
            """\
record("*", "PV") {
  alias("ALIAS_PV")
}
""",
            """\
record("*", "PV") {
    alias("ALIAS_PV")
}
""",
        ),
        (
            """\
record("*", "PV") {
 \tfield(DESC, "desc")
}
""",
            """\
record("*", "PV") {
    field(DESC, "desc")
}
""",
        ),
        (
            """\
record("*", "PV") {
    field(DESC, "desc")  # A comment

    field(VAL,  "val")
}
""",
            """\
record("*", "PV") {
    field(DESC, "desc")  # A comment
    field(VAL,  "val")
}
""",
        ),
        (
            """\
record("*", "PV") {
  field(DESC, "desc")
  info("info")
}
""",
            """\
record("*", "PV") {
    field(DESC, "desc")

    info("info")
}
""",
        ),
        (
            """\
 #Multiline
  #Comment
#Here
record("*", "PV") {
  field(DESC, "desc")


  #Multiline
    #Comment
  #Here



  info("info")
}
""",
            """\
#Multiline
#Comment
#Here
record("*", "PV") {
    field(DESC, "desc")

    #Multiline
    #Comment
    #Here
    info("info")
}
""",
        ),
        (
            """\
# Test comment

  ##########################################
   # Strange test block that engineers like #
##########################################

##########################################
   # MORE COMMENTS
record("*", "PV") {
    field(DESC, "desc")

  ###############
 # MORE BLOCKS #
   ###############
    info("info")
}
""",
            """\
# Test comment

##########################################
# Strange test block that engineers like #
##########################################

##########################################
# MORE COMMENTS
record("*", "PV") {
    field(DESC, "desc")

    ###############
    # MORE BLOCKS #
    ###############
    info("info")
}
""",
        ),
        (
            """\
record("*", "PV") {
    field(DESC, "desc")
    field( FO,  "FO")
    field( BAR,  "BAR")
    field(KAF, "KAF")
}
""",
            """\
record("*", "PV") {
    field(DESC, "desc")
    field(FO,   "FO")
    field(BAR,  "BAR")
    field(KAF,  "KAF")
}
""",
        ),
        (
            """\
record("*", "PV") {
    field(DOL,  "Foo#Bar")
}
""",
            """\
record("*", "PV") {
    field(DOL,  "Foo#Bar")
}
""",
        ),
        (
            """\
record("*", "PV") {
    field(FOO,  "some_value") # Comment
}
""",
            """\
record("*", "PV") {
    field(FOO,  "some_value")  # Comment
}
""",
        ),
        (
            """\
record("*", "PV") {
    field(FOO,  "$(P)$(R=)#$(N=0)PID-ON-proc PP")
}
""",
            """\
record("*", "PV") {
    field(FOO,  "$(P)$(R=)#$(N=0)PID-ON-proc PP")
}
""",
        ),
        (
            """\
record("*", "PV") {
    field(FOO,  "ad lorum ipsum info")
}
""",
            """\
record("*", "PV") {
    field(FOO,  "ad lorum ipsum info")
}
""",
        ),
        (
            """\
record("*", "PV") {
    # Comment record
    field(FOO,  "some_value") # Comment record
}
""",
            """\
record("*", "PV") {
    # Comment record
    field(FOO,  "some_value")  # Comment record
}
""",
        ),
    ],
    ids=[
        "Indenting depth before alias label",
        "Mixed indentation characters",
        "Comments after field definitions",
        "Indentation depth before info label",
        "Comment with \n before info",
        "Multiple comment blocks",
        "Leading spaces inside field and value alignment",
        "Internal (prefixed with #) record links",
        "Leading spaces before comment after unquoted field values",
        "Leading spaces before comment after complex field definition",
        "The word info in a field definition",
        "The word record in comment",
    ],
)
def test_extra_cases(initial, expected):
    actual, error = apply_rules(initial)
    assert actual == expected
    assert not error
