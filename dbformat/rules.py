"""Specific rules implementations."""

import re


def check_DB011(
    content: str, name: str, line_length: int = 80, quiet: bool = True
) -> bool:
    reg = re.compile(r"^\s*#.*")
    error = False
    for match in reg.finditer(content):
        if len(match[0]) > line_length:
            error = True
            if not quiet:
                print(f"Failed on rule DB011:{name}")
    return error


def sub_DB012(content: str, field_name_length: int = 4) -> str:
    regex = re.compile(r"field\((?:\s*)(?P<field>\w*),\s*(?P<value>.*)\)")

    def replace(match) -> str:
        return f"field({match.group('field') + ',':<{field_name_length+2}}{match.group('value')})"

    content = regex.sub(replace, content)
    return content


def sub_DB010(
    content: str,
) -> str:
    last_indent_regex = re.compile(r"\n(?P<last_indent>[ \t]*)")
    comment_regex = re.compile(
        r"(?P<new_line>\n)?(?P<actual_indent>[ \t]*)(?P<comment>#.*)$"
    )

    # Loop backwards on the content line by line. If find a comment line use
    # the indentation of the last line
    previous_line_idx = len(content)
    current_line_idx = content.rfind("\n", 0, previous_line_idx)
    while previous_line_idx != 0:
        comment_match = comment_regex.match(
            content, current_line_idx, previous_line_idx
        )
        if comment_match:
            match = last_indent_regex.match(content, previous_line_idx, len(content))
            if comment_match.group("actual_indent") != match.group("last_indent"):
                try:
                    subs = comment_regex.sub(
                        rf"\g<new_line>{match.group('last_indent')}\g<comment>",
                        content[current_line_idx:previous_line_idx],
                    )
                except IndexError:
                    # If last line didn't have any indentation
                    subs = comment_regex.sub(
                        r"\g<new_line>\g<comment>",
                        content[current_line_idx:previous_line_idx],
                    )
                content = (
                    content[:current_line_idx] + subs + content[previous_line_idx:]
                )
        previous_line_idx = current_line_idx
        try:
            current_line_idx = content.rindex("\n", 0, previous_line_idx)
        except ValueError:
            current_line_idx = 0

    return content


def sub_DB030(
    content: str,
) -> str:
    field = r"(?:field|alias|info)\([\w\d \",]+?\)"
    find_multiple_fields = re.compile(
        rf"(?P<indent>[ \t]+)(?P<field1>{field})(?:[ \t]*)(?P<field2>{field})"
    )
    while find_multiple_fields.search(content) is not None:
        content = find_multiple_fields.sub(
            r"\g<indent>\g<field1>\n\g<indent>\g<field2>", content
        )

    return content
