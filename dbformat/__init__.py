"""Functions for an EPICS database autoformatter, based on ESS style guidelines."""

import argparse
import json
import re
import sys
from pathlib import Path
from typing import Optional, Sequence, Tuple

if sys.version_info >= (3, 9):
    from importlib.metadata import version

    __version__ = version(__name__)
else:
    from pkg_resources import get_distribution

    __version__ = get_distribution(__name__).version

from dbformat import rules

GUIDELINES = json.loads((Path(__file__).parent / "guidelines.json").read_text())


def apply_rules(content: str, quiet: bool = True) -> Tuple[str, bool]:
    """Apply formatting and check all rules"""
    error = False
    for entry in GUIDELINES["guidelines"]:
        if not entry["rule"]:
            continue
        try:
            content = re.sub(
                entry["regex"]["pattern"],
                entry["regex"]["substitution"],
                content,
                flags=re.MULTILINE,
            )
        except KeyError:
            try:
                match = re.match(entry["regex"]["pattern"], content)
                if match:
                    error |= True
                    if not quiet:
                        print(f"Failed on rule {entry['id']}:{entry['name']}")
            except KeyError:
                if hasattr(rules, "sub_" + entry["id"]):
                    func = getattr(rules, "sub_" + entry["id"])
                    content = func(content)
                elif hasattr(rules, "check_" + entry["id"]):
                    func = getattr(rules, "check_" + entry["id"])
                    error |= func(content, entry["name"], quiet=quiet)
                else:
                    pass
    return content, error


def format_and_check(file: Path, quiet: bool = True) -> bool:
    initial = file.read_text()
    final, error = apply_rules(initial, quiet)
    if initial != final:
        file.write_text(final)
    return error


def print_guidelines() -> None:
    for entry in GUIDELINES["guidelines"]:
        print(
            f"{entry['id']} ({'Rule' if entry['rule'] else 'Recommendation'}): {entry['name']}"
        )


def main(argv: Optional[Sequence[str]] = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-V", "--version", action="version", version=f"{__name__} {__version__}"
    )
    parser.add_argument("filenames", nargs="*", type=Path, help="filenames to check")
    parser.add_argument(
        "--print-guidelines", action="store_true", help="print guidelines and exit"
    )
    parser.add_argument("--quiet", action="store_true", help="do not print errors")
    args = parser.parse_args()

    if args.print_guidelines:
        print_guidelines()
        return 0

    retval = 0
    for filename in args.filenames:
        try:
            retval += format_and_check(filename, args.quiet)
        except Exception as exc:
            print(f"{filename}: {exc}")
            retval += 1
    return retval


if __name__ == "__main__":
    raise SystemExit(main())
